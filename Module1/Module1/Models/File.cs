using System.ComponentModel.DataAnnotations;

namespace Module1.Models
{
    public class File
    {
        public int Id { get; set; }        
        public string OriginName { get; set; }
        public int DownloadCount { get; set; }     
        public string Link { get; set; }        
        public string Password { get; set; }
        
        public int Count { get; set; }
        public string Name { get; set; }       
        public string Path { get; set; }       
        public string Content { get; set; }       
        public string ContentType { get; set; }       
        public string Description { get; set; }
    }
}