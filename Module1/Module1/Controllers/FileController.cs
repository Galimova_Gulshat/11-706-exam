using System;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Module1.Data;
using File = Module1.Models.File;

namespace Module1.Controllers
{
    public class FileController : Controller
    {
        private readonly DatabaseContext _context;
        private readonly IHostingEnvironment _appEnvironment;

        public FileController(DatabaseContext context, IHostingEnvironment appEnvironment)
        {
            _context = context;
            _appEnvironment = appEnvironment;
        }
        public IActionResult Index()
        {
            return View(_context.Files);
        }
        
        public IActionResult Create()
        {
            return View();
        }
        
        [HttpPost]
        public IActionResult Create(IFormFile formFile, string name, string description, string link, string password, int count)
        {
            if (!ModelState.IsValid || count < 0)
                return View();
            var file = new File();
            if (formFile != null && formFile.Length > 0)
            {
                file.Name = name == null ? formFile.FileName : name;
                file.OriginName = formFile.FileName;
                file.Description = description;
                if (link != null && !link.Equals(string.Empty))
                {
                    if (_context.Files.FirstOrDefault(c => c.Link == link) == default(File))
                        file.Link = link;
                    else
                    {
                        ModelState.AddModelError("", "Link should be unique");
                        return View();
                    }
                }
                else file.Link = GetLink(file.OriginName);
                if (password != null)
                    file.Password = password;
                var filePath = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files"));
                using (var fileStream = new FileStream(Path.Combine(filePath, file.OriginName), FileMode.Create))
                    formFile.CopyTo(fileStream);
                file.Path = "wwwroot/Files/"+ file.OriginName;
                file.ContentType = formFile.ContentType;
                var readStream = formFile.OpenReadStream();
                byte[] bytes = new byte[readStream.Length];                
                readStream.Read(bytes, 0, Convert.ToInt32(readStream.Length));
                var strBase64 = Convert.ToBase64String(bytes);
                var content = string.Format("data:" + file.ContentType + ";base64,{0}", strBase64);
                file.Content = content;
                file.Count = count;
                _context.Files.Add(file);
                _context.SaveChanges();
                return RedirectToAction("Index");
                
            }

            return View();

        }

        private string GetLink(string chars)
        {
            var random = new Random();
            var builder = new StringBuilder();
            builder.Append("https://");
            for (int i = 0; i < 6; i++)
            {
                var pos = random.Next(0, chars.Length - 1);
                builder.Append(chars[pos]);
            }

            return builder.ToString();
        }

       
        public IActionResult Show(int id)
        {
            return View(_context.Files.First(f => f.Id == id));
        }

        public IActionResult GetFile(int id)
        {
            var file = _context.Files.FirstOrDefault(f => f.Id == id);
            string file_path = Path.Combine(_appEnvironment.ContentRootPath, file.Path);
            string file_type = file.ContentType;
            file.DownloadCount++;
            file.Count--;
            _context.SaveChanges();
            return PhysicalFile(file_path, file_type, file.OriginName);
        }
        
        [HttpPost]
        public IActionResult GetFileByPassword(int id, string password)
        {
            if (_context.Files.FirstOrDefault(f => f.Id == id && f.Password == password) == default(File))
                return RedirectToAction("Index");
            return GetFile(id);
        }
    }
}