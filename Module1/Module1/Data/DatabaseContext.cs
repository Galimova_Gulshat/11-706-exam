using Microsoft.EntityFrameworkCore;
using Module1.Models;

namespace Module1.Data
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext() : base() {}
        
        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options)
        { }
        
        public DbSet<File> Files { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=EFProviders.InMemory;Trusted_Connection=True;ConnectRetryCount=0");
            }
        } 
    }
}