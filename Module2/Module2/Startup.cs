﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Module2.Data;
using Module2.Models;

namespace Module2
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.AddDbContext<DatabaseContext>(opt => opt.UseInMemoryDatabase());
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.LoginPath = new Microsoft.AspNetCore.Http.PathString("/Auth/Register");
                });
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, DatabaseContext context)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            CreateUserRoles(context).Wait();
        }

        private static async Task CreateUserRoles(DatabaseContext context)  
        {  
            string adminRoleName = "admin";
            string userRoleName = "user";
 
            string adminEmail = "admin@mail.ru";
            string adminPassword = "123456";
 
            // добавляем роли
            Role adminRole = new Role { Name = adminRoleName };
            Role userRole = new Role { Name = userRoleName };
            User adminUser = new User { Email = adminEmail, Password = adminPassword, RoleId = adminRole.Id };
            Restaurant res = new Restaurant {Id = 1, Name = "Home"};
            Dish dish = new Dish {Id = 1, Name = "q", Price = 100, Description = "Cool", RestaurantId = 1};
            Dish dish2 = new Dish {Id = 2, Name = "Cake", Price = 150, Description = "Cool cake", RestaurantId = 1};
 
            context.Roles.AddRange(new Role[] { adminRole, userRole });
            context.Users.AddRange( new User[] { adminUser });
            context.Restaurants.AddRange( new Restaurant[] { res });
            context.Dishes.AddRange( new Dish[] { dish, dish2 });
            context.SaveChanges();
        } 
    }
}