namespace Module2.Models
{
    public class DishInBasket
    {
        public int Id { get; set; }
        public int BasketId { get; set; }
        public int DishId { get; set; }
    }
}