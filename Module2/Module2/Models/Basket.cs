namespace Module2.Models
{
    public class Basket
    {
        public int Id { get; set; }
        public string UserEmail { get; set; }
        public int RestaurantId { get; set; }
        
        public int Price { get; set; }
    }
}