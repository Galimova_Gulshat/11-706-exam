namespace Module2.Models
{
    public class Order
    {
        public int Id { get; set; }
        
        public string Email { get; set; }
        public int BasketId { get; set; }
        public int Price { get; set; }
    }
}