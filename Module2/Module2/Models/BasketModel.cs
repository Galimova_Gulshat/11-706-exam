using System.Collections.Generic;

namespace Module2.Models
{
    public class BasketModel
    {
        public int Price { get; set; }
        public IEnumerable<Dish> Dishes { get; set; }
    }
}