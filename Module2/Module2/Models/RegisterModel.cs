using System.ComponentModel.DataAnnotations;

namespace Module2.Models
{
    public class RegisterModel
    {
        [Required(ErrorMessage ="Не указан Email")]
        [EmailAddress (ErrorMessage = "Некорректный адрес")]
        public string Email { get; set; }
         
        [Required(ErrorMessage = "Не указан пароль")]
        [StringLength(10, MinimumLength = 6, ErrorMessage = "Длина строки должна быть от 6 до 10 символов")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
         
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Пароль введен неверно")]
        public string ConfirmPassword { get; set; }
    }
}