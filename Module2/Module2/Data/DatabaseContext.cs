using Microsoft.EntityFrameworkCore;
using Module2.Models;

namespace Module2.Data
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext() : base() {}
        
        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options)
        { }
        
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Restaurant> Restaurants { get; set; }
        public DbSet<Dish> Dishes { get; set; }
        public DbSet<Basket> Baskets { get; set; }
        public DbSet<DishInBasket> DishesInBasket { get; set; }
        public DbSet<Order> Orders { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=EFProviders.InMemory;Trusted_Connection=True;ConnectRetryCount=0");
            }
        } 
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            string adminRoleName = "admin";
            string userRoleName = "user";
 
            string adminEmail = "admin@mail.ru";
            string adminPassword = "123456";
 
            // добавляем роли
            Role adminRole = new Role { Id = 1, Name = adminRoleName };
            Role userRole = new Role { Id = 2, Name = userRoleName };
            User adminUser = new User { Id = 1, Email = adminEmail, Password = adminPassword, RoleId = adminRole.Id };
            Restaurant res = new Restaurant {Id = 1, Name = "Home"};
            Dish dish = new Dish {Id = 1, Name = "q", Price = 100, Description = "Cool", RestaurantId = 1};
            Dish dish2 = new Dish {Id = 2, Name = "Cake", Price = 150, Description = "Cool cake", RestaurantId = 1};
 
            modelBuilder.Entity<Role>().HasData(new Role[] { adminRole, userRole });
            modelBuilder.Entity<User>().HasData( new User[] { adminUser });
            modelBuilder.Entity<Restaurant>().HasData( new Restaurant[] { res });
            modelBuilder.Entity<Dish>().HasData( new Dish[] { dish, dish2 });
            base.OnModelCreating(modelBuilder);
        }
    }
}