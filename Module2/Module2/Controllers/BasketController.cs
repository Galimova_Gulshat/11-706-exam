using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Module2.Data;
using Module2.Models;

namespace Module2.Controllers
{
    [Authorize(Roles = "user")]
    public class BasketController : Controller
    {
        private readonly DatabaseContext _context;

        public BasketController(DatabaseContext context)
        {
            _context = context;
        }
        public IActionResult Add(int id)
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");
            var dish = _context.Dishes.FirstOrDefault(d => d.Id == id);
            var basket = _context.Baskets.FirstOrDefault(b => b.UserEmail == User.Identity.Name);
            if (basket == default(Basket))
            {
                var newBasket = new Basket
                    {RestaurantId = dish.RestaurantId, UserEmail = User.Identity.Name};
                _context.Baskets.Add(newBasket);
                basket = newBasket;
            }
            else if (basket.RestaurantId != dish.RestaurantId)
            {
                _context.Baskets.Remove(basket);
                _context.DishesInBasket.RemoveRange(_context.DishesInBasket.Where(d => d.BasketId == basket.Id));
                var newBasket = new Basket
                    {RestaurantId = dish.RestaurantId, UserEmail = User.Identity.Name};
                _context.Baskets.Add(newBasket);
                basket = newBasket;
            }
            _context.DishesInBasket.Add(new DishInBasket
                {BasketId = basket.Id, DishId = id});
            basket.Price += dish.Price;
            _context.SaveChanges();
            return RedirectToAction("Index", "Home");
        }

        public IActionResult Show()
        {
            var basket = _context.Baskets.FirstOrDefault(b => b.UserEmail == User.Identity.Name);
            var dishesId = _context.DishesInBasket.Where(d => d.BasketId == basket.Id);
            var dishes = _context.Dishes.Join(dishesId, x => x.Id, y => y.DishId, (x, y) => x);
            var model = new BasketModel{Price = basket.Price, Dishes = dishes};
            return View(model);
        }

        public IActionResult Order()
        {
            var basket = _context.Baskets.FirstOrDefault(b => b.UserEmail == User.Identity.Name);
            var order = new Order{ Price = basket.Price };
            _context.Orders.Add(order);
            _context.Baskets.Remove(basket);
            _context.SaveChanges();
            return RedirectToAction("Index", "Home");
        }
    }
}