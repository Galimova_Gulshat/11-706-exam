using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Module2.Data;
using Module2.Models;

namespace Module2.Controllers
{
    public class AdminController : Controller
    {
        private readonly DatabaseContext _context;

        public AdminController(DatabaseContext context)
        {
            _context = context;
        }

        public IActionResult ShowRestaurants()
        {
            return View(_context.Restaurants);
        }
        
        public IActionResult Create() => View();
        [HttpPost]
        public async Task<IActionResult> Create(string name)
        {
            if(_context.Restaurants.FirstOrDefault(r => r.Name == name) == default(Restaurant))
                return View();
            _context.Restaurants.Add(new Restaurant {Name = name});
            _context.SaveChanges();
            return RedirectToAction("ShowRestaurants");
        }
         
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            _context.Restaurants.Remove(_context.Restaurants.FirstOrDefault(r => r.Id == id));
            _context.SaveChanges();
            return RedirectToAction("ShowRestaurants");
        }
        
        public async Task<IActionResult> DeleteDish(int id)
        {
            _context.Dishes.Remove(_context.Dishes.FirstOrDefault(r => r.Id == id));
            _context.DishesInBasket.RemoveRange(_context.DishesInBasket.Where(d => d.DishId == id));
            _context.SaveChanges();
            return RedirectToAction("ShowRestaurants");
        }
    }
}